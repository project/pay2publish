INSTALLATION:

1. Install the module
2. Create a 'Levels' (or any other name) vocabulary under /admin/category
3. Add to the vocabulary your levels categories (es: basic, silver, gold)
4. Go to /admin/taxonomyactions and click the 'Add Actions' tab to create your levels (choose name, select content types, default and upgrade level, price)
5. Create the needed duration times (in months) by clicking on the 'Add Duration' tab

Now the content types affected by the actions will get the basic level attached to them by default. The level can 
be upgraded upon the payment of the price you setted up.

To decide which fields to display for each level, you need to act in your node-content_type.tpl.php file:

<?php foreach ((array)$taxonomy_actions as $act)
if ($act->payment > 0){ ?>

  <?php if (($act->vid == 3)or($act->vid == 1)or($act->vid == 2)) {?>
	<h3 class="field-label">Field Basic</h3>
	<?php print $item['view'] ?>
  <?php } ?>

  <?php if (($act->vid == 1)or($act->vid == 2)) {?>
	<h3 class="field-label">Field Silver</h3>
	<?php print $item['view'] ?>
  <?php } ?>

    <?php if ($act->vid == 2) {?>
	<h3 class="field-label">Field Gold</h3>
	<?php print $item['view'] ?>
    <?php }?>

<?php }?>
