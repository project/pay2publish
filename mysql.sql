-- phpMyAdmin SQL Dump
-- version 2.7.0-pl2
-- http://www.phpmyadmin.net
-- 
-- Хост: localhost
-- Время создания: Сен 25 2006 г., 19:40
-- Версия сервера: 4.1.11
-- Версия PHP: 4.3.11
-- 
-- БД: `visual2_kite`
-- 

-- --------------------------------------------------------

-- 
-- Структура таблицы `taxonomy_actions`
-- 

CREATE TABLE `taxonomy_actions` (
  `vid` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `description` longtext,
  `taxtermstart` int(11) NOT NULL default '0',
  `taxtermend` int(11) NOT NULL default '0',
  `cattermstart` int(11) NOT NULL default '0',
  `cattermend` int(11) NOT NULL default '0',
  `duration` int(11) NOT NULL default '0',
  `cost` int(11) NOT NULL default '0',
  `weight` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`vid`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

-- 
-- Структура таблицы `taxonomy_actions_duration`
-- 

CREATE TABLE `taxonomy_actions_duration` (
  `vid` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `description` longtext,
  `duration` int(11) NOT NULL default '0',
  `weight` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`vid`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

-- 
-- Структура таблицы `taxonomy_actions_node`
-- 

CREATE TABLE `taxonomy_actions_node` (
  `tid` int(11) NOT NULL default '0',
  `nid` int(10) unsigned NOT NULL default '0',
  `vid` int(10) unsigned NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `act` varchar(255) NOT NULL default '',
  `duration_m` int(11) NOT NULL default '0',
  `duration` int(11) NOT NULL default '0',
  `cost` int(11) NOT NULL default '0',
  `payment` int(11) NOT NULL default '0',
  `weight` int(11) NOT NULL default '0',
  PRIMARY KEY  (`tid`),
  KEY `nid` (`nid`),
  KEY `tid` (`vid`),
  KEY `uid` (`uid`),
  KEY `vid` (`vid`),
  KEY `payment` (`payment`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

-- 
-- Структура таблицы `taxonomy_actions_node_types`
-- 

CREATE TABLE `taxonomy_actions_node_types` (
  `vid` int(10) unsigned NOT NULL default '0',
  `type` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`vid`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
